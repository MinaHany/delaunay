module Main where

import Delaunay
import System.Environment (getArgs)
import Data.List.Split (splitOn)

readSplit :: String => (Double, Double)
readSplit s = ( (read x)::Double , (read y)::Double )
    where x    = list !! 0 
          y    = list !! 1
          list = splitOn "," s

main :: IO ()
main = do
    args <- getArgs
    let nargs = (map readSplit args)
    print $ triangulate nargs
