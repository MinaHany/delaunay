--C implementation from http://paulbourke.net/papers/triangulate/triangulate.c
module Delaunay where

import Data.List
import Data.Set
import qualified Data.Set as S
import Debug.Trace

type Point    = (Double, Double)
type Edge     = (Point, Point)
type Triangle = (Point, Point, Point)
type MinMax   = ((Double, Double), (Double, Double)) -- ((xmin, xmax), (ymin, ymax))

epsilon :: Double
epsilon = 0.000001

fstT :: Triangle -> Point
fstT (x,_,_) = x

sndT :: Triangle -> Point
sndT (_,y,_) = y

trdT :: Triangle -> Point
trdT (_,_,z) = z       

xsort :: Point -> Point -> Ordering
xsort (x1, y1) (x2, y2) | x1 < x2   = LT
                        | x1 > x2   = GT
                        | otherwise = EQ
                        
(===) :: Point -> Point -> Bool
(x1, y1) === (x2, y2) = x1 == x2 && y1 == y2

shareVertex :: Triangle -> Triangle -> Bool
shareVertex (p1, p2, p3) (p4, p5, p6) = p1 === p4 || p1 === p5 || p1 === p6 ||
                                        p2 === p4 || p2 === p5 || p2 === p6 ||
                                        p3 === p4 || p3 === p5 || p3 === p6
                     
makeTriangle :: Point -> Point -> Point -> Triangle
makeTriangle x y z = (x, y, z)

edgesOf :: Triangle -> [Edge]
edgesOf (p1, p2, p3) = [e1, e2, e3]
    where
        e1 = if p1 < p2 then (p1,p2) else (p2,p1) 
        e2 = if p2 < p3 then (p2,p3) else (p3,p2)
        e3 = if p1 < p3 then (p1,p3) else (p3,p1)

----------------------------------------------------------------------------------------------
-- generates the super triangle that contains all the other points
superTriangle :: [Point] -> Triangle
superTriangle xs = superTriangle' $ minmax xs

superTriangle' :: MinMax -> Triangle
superTriangle' (x, y) = makeTriangle (xmid - 20*dmax, ymid - dmax   ) 
                                     (xmid          , ymid + 20*dmax) 
                                     (xmid + 20*dmax, ymid - dmax   )
    where
        xmin = fst x
        xmax = snd x
        ymin = fst y
        ymax = snd y
        dx   = xmax - xmin
        dy   = ymax - ymin
        dmax = max dx dy
        xmid = (xmin + xmax) / 2
        ymid = (ymin + ymax) / 2

minmax :: [Point] -> MinMax
minmax xs = minmax' xs (fst $ head xs) (fst $ head xs) (snd $ head xs) (snd $ head xs)

minmax' :: [Point] -> Double -> Double -> Double -> Double -> MinMax
minmax' []     xmin xmax ymin ymax = ((xmin, xmax), (ymin, ymax))
minmax' (x:xs) xmin xmax ymin ymax = minmax' xs xmin' xmax' ymin' ymax'
    where
        xmin' = min xmin $ fst x
        xmax' = max xmax $ fst x
        ymin' = min ymin $ snd x
        ymax' = max ymax $ snd x
----------------------------------------------------------------------------------------------
inCircumCircle :: Point -> Triangle -> Bool
inCircumCircle (x,y) (p1, p2, p3) | absy1y2 < epsilon && absy2y3 < epsilon = False
                                  | (drsqr - rsqr) > epsilon               = False
                                  | otherwise                              = True
    where
        absy1y2 = abs $ (snd p1) - (snd p2)
        absy2y3 = abs $ (snd p2) - (snd p3)
        drsqr   = (x - xc)*(x - xc) + (y - yc)*(y - yc)
        rsqr    = (fst p2 - xc)*(fst p2 - xc) + (snd p2 - yc)*(snd p2 - yc)
        (xc,yc) = computeCenter absy1y2 absy2y3 (p1, p2, p3)

computeCenter :: Double -> Double -> Triangle -> (Double, Double)
computeCenter abs12 abs23 ((x1,y1), (x2,y2), (x3,y3)) | abs12 < epsilon = (xc12, yc12)
                                                      | abs23 < epsilon = (xc23, yc23)
                                                      | otherwise       = (xc  , yc  )
    where
        xc12 = (x2 + x1) / 2
        yc12 = m2 * (xc12 - mx2) + my2
        xc23 = (x3 + x2) / 2
        yc23 = m1 * (xc23 - mx1) + my1
        xc   = (m1 * mx1 - m2 * mx2 + my2 - my1) / (m1 - m2)
        yc   = if abs12 > abs23 then m1 * (xc - mx1) + my1 else m2 * (xc - mx2) + my2
        m1   = - (x2-x1) / (y2-y1)
        m2   = - (x3-x2) / (y3-y2)
        mx1  = (x1 + x2) / 2.0
        mx2  = (x2 + x3) / 2.0
        my1  = (y1 + y2) / 2.0
        my2  = (y2 + y3) / 2.0
----------------------------------------------------------------------------------------------
filterTriangles :: Triangle -> [Triangle] -> [Triangle]
filterTriangles super []     = []
filterTriangles super (x:xs) = if shareVertex super x then filterTriangles super xs else x:(filterTriangles super xs)

reTriangulate :: [Triangle] -> [Edge] -> Point -> [Triangle]
reTriangulate curr []     p = curr
reTriangulate curr (x:xs) p = trace ("newT: " ++ show (fst x, snd x, p) ++ "\n buf: " ++ show (x:xs)) (reTriangulate (curr ++ [(fst x, snd x, p)]) xs p)

--          current       accumulator   buffer
addPoint :: [Triangle] -> [Triangle] -> Set Edge -> Point -> [Triangle]
addPoint []     acc buf p = reTriangulate acc (S.toList buf) p
addPoint (x:xs) acc buf p = if inCircumCircle p x 
                            then trace (show p ++ " inside " ++ show x) (addPoint xs acc (S.union (S.fromList $ edgesOf x) buf) p)
                            else addPoint xs (acc ++ [x]) buf p

addPoints :: [Triangle] -> [Point] -> [Triangle]
addPoints curr []     = curr
addPoints curr (x:xs) = addPoints (addPoint curr [] S.empty x) xs 

triangulate :: [Point] -> [Triangle]
triangulate pts | length pts <  3 = []
                | length pts == 3 = [(pts!!0, pts!!1, pts!!2)]
                | otherwise       = filterTriangles s $ addPoints [s] $ (sortBy xsort pts) ++ [fstT s, sndT s, trdT s]
    where
        s = superTriangle pts
