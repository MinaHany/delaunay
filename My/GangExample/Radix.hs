{-# LANGUAGE CPP #-}
module Radix where

import Data.Vector.Unboxed   (Vector, Unbox, MVector, indexM, (!))
import qualified Data.Vector.Unboxed  as U (length, slice, mapM_, foldl)
import qualified Data.Vector.Generic.Mutable as M
import qualified Data.Vector as B
import GHC.ST 
import Data.Word
import Data.Bits
import Gang

import Debug.Trace



-- Assumes that the Bits.bitsize function is defined, returns a multiple of 8 
-- and that the binary representation reflects the order
sortBy:: 
  Bits b => (a -> b) -> B.Vector a -> B.Vector a
sortBy toBitrep vec = runST  $ do 
  { bitRep <- B.unsafeThaw $ B.map toBitrep vec
  ; return undefined
}


  


-- counters: 
--   has 256 * gangSize entries
--   nth entry contains histogram of thread n before scan, offsets after scan
--
--   toggles dst and src
radixSort64::
  Gang -> (MVector s Word64) -> (MVector s Word64) ->  (MVector s Int) -> Int  -> ST s ()
radixSort64 gang src dst counters vecSize = do 
  { let (mask :: Word64) = 255
  ; mapM_ (radixSort' mask src dst counters) [0..7]
  }
  where 
    radixSort' mask src dst counters byteNo
      | (even byteNo) = sortOne gang (shift mask (8*byteNo)) src dst counters vecSize byteNo
      | otherwise     = sortOne gang (shift mask (8*byteNo)) dst src counters vecSize byteNo


sortOne::
  Gang  -> Word64 -> (MVector s Word64) -> (MVector s Word64) ->  (MVector s Int) -> Int  -> Int -> ST s ()
sortOne  gang flag src dst counters vecSize byteNo = do
  { gangST gang "resetCounter" WorkUnknown (resetCounter counters (gangSize gang))
  ; gangST gang "countBits" WorkUnknown (countBits flag src counters (gangSize gang) byteNo)
  ; vstr <- vecTraceStr counters (256 * gangSize gang)
  ; -- trace (vstr ++ "\n" ) 
    mvecPreScanTr counters (256 * (gangSize gang)) (gangSize gang)
  ; gangST gang "sortBits" WorkUnknown (sortBits flag src dst counters (gangSize gang) byteNo)
  }





mvecPreScanTr :: (MVector s Int) -> Int  -> Int ->  ST s ()
mvecPreScanTr mvec size gs = do 
  prescan 0 0 0
  return ()
  where
    prescan !bucket !thread !pre 
      | bucket == 256 = return ()  
      | thread == gs  = prescan (bucket+1) 0 pre
      | otherwise     = do 
          { let ind = thread * 256 + bucket
          ; v <- M.read mvec ind
          ; M.write mvec ind pre
          ; prescan bucket (thread+1) (pre + v)
          }

-- count number of items where flag bit is zero/one respectively in the src
-- vector                 
countBits:: 
  Word64 -> (MVector s Word64) -> (MVector s Int) -> Int -> Int  -> Int -> ST s ()
countBits mask src cntVec gs byteNo threadId = do
  { let l = M.length src
  ; let maxChunkSize = (l + gs - 1) `div` gs
  ; let chunkSize = min maxChunkSize (l - maxChunkSize * threadId)
  ; let ind = threadId * maxChunkSize
  ; mapM_ updateCounter [ind..(ind + chunkSize-1)]
  }
  where
--     updateCounter :: Int -> ST s ()
     updateCounter ind = do 
       { v <- M.read src ind
       ; let bucket = fromInteger $  toInteger $ shift (v .&. mask) (-1 * 8 * byteNo)
       ; let cntInd = -- trace ((show bucket) ++ " " ++ (show v)) $ 
                      -- bucket * gs + threadId
                      threadId * 256 + bucket
       ; let cntInd = -- trace ((show bucket) ++ " " ++ (show v)) $ 
                      threadId * 256 + bucket
       ; cnt <- M.read cntVec cntInd    
       ; -- trace  ("t " ++ (show (threadId, bucket, gs, v)) ++ " write " ++(show cnt) ++ " +1 to " ++ (show cntInd)) $
         M.write cntVec cntInd (cnt + 1)
       }


resetCounter cntVec gs threadId = do
  { let ind = threadId * 256
  ; mapM_ (\i -> M.write cntVec i 0) [ind..(ind + 255)] 
  }

sortBits:: 
  Word64 -> (MVector s Word64) -> (MVector s Word64) -> (MVector s Int) -> Int -> Int -> Int -> ST s ()
sortBits mask src dst offsets gs byteNo threadId = do
  { let ind = threadId * maxChunkSize
  ; mapM_ copySorted [ind..(ind + chunkSize-1)]
  }
  where    
    l            = M.length src
    maxChunkSize = (l + gs - 1) `div` gs
    chunkSize = min maxChunkSize (l - maxChunkSize * threadId)
    copySorted ind = do
      { v <- M.read src ind
      ; let bucket = fromInteger $  toInteger $ shift (v .&. mask) (-1 * 8 * byteNo)
      ; let cntInd = -- trace ((show bucket) ++ " " ++ (show v)) $ 
                     -- bucket * gs + threadId
                     threadId * 256 + bucket
      ; offset <- M.read offsets cntInd
      ; M.write dst offset v      
      ; M.write offsets cntInd (offset+1) 
      }

-- vecTraceStr :: (M.MVector MVector a, Show a) => MVector s a -> Int -> ST s String
vecTraceStr vec size =
  vecTrace' 0 
  where
    vecTrace' n 
      | n == size = return ""
      | otherwise = do {v <- M.read vec n; 
                       ; str <- vecTrace' (n+1)
                       ; return ((show v) ++ " " ++ str)
                     }