module Main where

import Gang
import Radix
import Timing
import qualified Data.Vector.Unboxed as U
import qualified Data.Vector.Generic.Mutable as M
import GHC.ST
import System.IO.Unsafe 
import Control.Concurrent
import Control.Monad.ST.Unsafe
-- import Control.DeepSeq
import Data.Word

import System.Environment


readVec ::  Int -> IO (U.Vector Word64)
readVec size = do
    v  <- M.new size
    fill v 0 
    U.unsafeFreeze v
  where
    fill v i 
        | i < size = do
            x <- readLn 
            M.unsafeWrite v i x
            -- putStrLn ("Writen " ++ (show x))
            fill v (i+1) 
        | otherwise = return ()

theGang :: Gang
theGang = unsafePerformIO (getNumCapabilities >>= forkGang)
{-# NOINLINE theGang #-}


-- mainST :: Word64 -> ST s0 (U.MVector s0 Word64)
mainST  size src = do 
  { scanVec <- M.new (256 * gangSize theGang) -- U.thaw $ U.generate (gangSize theGang) (\i -> (0, 0))
  ; srcVec  <- U.unsafeThaw src -- $ U.generate size (\i -> (s - (fromInteger $ toInteger i)::Word64))
  ; dstVec  <- M.new size -- U.thaw $ U.generate size (\i -> 0::Word64)
  -- enforce evaluation of mvec, ivec
  ; x1 <- M.read scanVec 0
--  ; (x1, _) <- M.read scanVec 0
  ; x2 <- M.read srcVec 0
  ; x3 <- M.read dstVec 0
  ; x1 `seq` x2 `seq` x2  `seq` return ()
  ; (_, t) <- timeST $ radixSort64 theGang srcVec dstVec scanVec size
  ; mv <- U.unsafeFreeze dstVec
  ; return (mv, t)
  }

main :: IO ()
main  = do 
  { size <- readLn
  ; putStrLn $ show size
  ; sv <- readVec  size 
  ; let (v, t) = runST $ mainST size sv
  ; putStr	$ prettyTime t
  ; putStrLn $ show $ (take 20) (U.toList v) 
  }