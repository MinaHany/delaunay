module Delaunay where

import Data.IORef
import Data.List

data Vertex   = Vtx { x :: Double,
                      y :: Double,
                      inT :: IORef Triangle,
                      vertexId :: Int }
                      
data Triangle = Tri { ngh :: [IORef Triangle],
                      vtx :: [IORef Vertex], 
                      tid :: Int, 
                      bad :: Bool }
                      
data Simplex = Spl { tri      :: IORef Triangle,
                     o        :: Int,
                     boundary :: Bool }                     

setNeighboursT :: Triangle -> Triangle -> Triangle -> Triangle -> IO ()
setNeighboursT o a b c = do writeIORef (n !! 0) a
                            writeIORef (n !! 1) b
                            writeIORef (n !! 2) c
                         where n = ngh o

setVerticesT :: Triangle -> Vertex -> Vertex -> Vertex -> IO ()
setVerticesT o a b c = do writeIORef (v !! 0) a
                          writeIORef (v !! 1) b
                          writeIORef (v !! 2) c
                       where v = vtx o

locateT :: Triangle -> IORef Triangle -> Int
locateT o x = case elemIndex x (ngh o) of
                                    Just n  -> n
                                    Nothing -> error "Could not locate"

updateT :: Triangle -> IORef Triangle -> Triangle -> IO ()
updateT o x y = do writeIORef (n !! pos) y
               where n   = ngh o
                     pos = locateT o x
                     
mod3 :: Int -> Int
mod3 x = if x > 2 then x - 3 else x

validS :: Simplex -> Bool
validS s = not $ boundary s

isTriangleS :: Simplex -> Bool
isTriangleS s = not $ boundary s

isBoundaryS :: Simplex -> Bool
isBoundaryS s = boundary s

firstVertexS :: Simplex -> IORef Vertex
firstVertexS s = do t <- tri s
                    return ((vtx t) !! (o s))

rotClockwise :: Simplex -> Simplex
rotClockwise s =                                  
