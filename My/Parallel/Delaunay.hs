{-# LANGUAGE TemplateHaskell, TypeOperators #-}
module Delaunay where

import Control.Category
import Data.Label
import Prelude hiding ((.), id)
import Helpers
import qualified KdTree as KT
import Data.Array.IO
--import Gang

data Boundary = Boundary { _center :: Point,
                           _half :: Point }

data QTree = Node { _ne :: QTree,
                    _nw :: QTree,
                    _sw :: QTree,
                    _se :: QTree }
           | Leaf

data Vertex   = Vertex { _x   :: Double,
                         _y   :: Double,
                         _inT :: Maybe Triangle }

data Triangle = Triangle { _ngh :: IOArray Int (Maybe Triangle),
                           _vtx :: IOArray Int Vertex, 
                           _tid :: Int, 
                           _bad :: Bool }
                      
data Simplex = Simplex { _tri      :: Triangle,
                         _o        :: Int,
                         _boundary :: Bool } 
                         
data Qs = Qs { _vertexQ  :: IOArray Int Vertex,
               _simplexQ :: IOArray Int Simplex }

data Qslist = Qslist { _list :: IOArray Int Qs }

mkLabels [''Boundary, ''QTree, ''Vertex, ''Triangle, ''Simplex, ''Qs, ''Qslist]

triangleArea :: Point -> Point -> Point -> Double
triangleArea (ax, ay) (bx, by) (cx, cy) = (cx*by-bx*cy)-(cx*ay-ax*cy)+(bx*ay-ax*by);

containsPoint :: Boundary -> Point -> Bool
containsPoint boundary (px, py) | a1 > 0 && a2 > 0 && a3 > 0 && a4 > 0 = True
                                | a1 < 0 && a2 < 0 && a3 < 0 && a4 < 0 = True
                                | otherwise = False
    where a1 = triangleArea a b (px, py)
          a2 = triangleArea b c (px, py)
          a3 = triangleArea c d (px, py)
          a4 = triangleArea a d (px, py)
          a  = (px + hx, py + hy)
          b  = (px - hx, py + hy)
          c  = (px - hx, py - hy)
          d  = (px + hx, py - hy)
          (px, py) = get center boundary
          (hx, hy) = get half boundary

--1: ne, 2: nw, 3: sw, 4: se
--points on x or y axis must be handled
quadrant :: Point -> Boundary -> Int
quadrant (px, py) b | px > cx && py > cy = 1
                    | px < cx && py > cy = 2 
                    | px < cx && py < cy = 3
                    | px > cx && py < cy = 4
                    | otherwise = error "Duplicate Point"
    where (cx, cy) = get center b

boundarySize = 10

v2p :: Vertex -> Point
v2p v = ((get x v), (get y v))

setV :: Triangle -> Vertex -> Vertex -> Vertex -> IO () 
setV t v1 v2 v3 = do let arr = get vtx t
                     writeArray arr 1 v1
                     writeArray arr 2 v2
                     writeArray arr 3 v3

setT :: Triangle -> Maybe Triangle -> Maybe Triangle -> Maybe Triangle -> IO ()
setT t t1 t2 t3 = do let arr = get ngh t
                     writeArray arr 1 t1
                     writeArray arr 2 t2
                     writeArray arr 3 t3       
               
generateBoundary :: [Point] -> Int -> Int -> IOArray Int Vertex -> [Triangle] -> IO Simplex
generateBoundary p n bCount v ts = do (lo, hi) <- getBounds v
                                      writePoints v center boundarySize radius (hi - bCount + 1, hi)
                                      let t = ts !! (2 * n)
                                      v1 <- readArray v $ hi - bCount + 1
                                      v2 <- readArray v $ hi - bCount + 2
                                      v3 <- readArray v $ hi - bCount + 3
                                      return $ set inT (Just t) v1
                                      return $ set inT (Just t) v2
                                      return $ set inT (Just t) v3
                                      writeArray v (hi - bCount + 1) v1
                                      writeArray v (hi - bCount + 2) v2
                                      writeArray v (hi - bCount + 3) v3
                                      setT t Nothing Nothing Nothing
                                      setV t v1 v2 v3
                                      let s = Simplex t 0 False
                                      extend s v ts (2 * n) (hi - bCount + 4, hi)
                                      return s       
    where (minP, maxP) =  minmax p
          size    = Helpers.length (maxP |-| minP)
          stretch = 10
          radius  = stretch * size
          center  = maxP |+| ((maxP |-| minP) |/| 2)
          
writePoints :: IOArray Int Vertex -> Point -> Int -> Double -> (Int,Int) -> IO ()
writePoints arr center bCount radius (lo, hi) = if lo > hi 
                                                then return () 
                                                else do let cmp = (2 * pi * (fromIntegral lo)) / (fromIntegral bCount)
                                                        let x   = radius * cos(cmp)
                                                        let y   = radius * sin(cmp)
                                                        writeArray arr lo $ Vertex (x + fst center) (y + snd center) Nothing 
                                                        writePoints arr center bCount radius (lo + 1, hi)

extend :: Simplex -> IOArray Int Vertex -> [Triangle] -> Int -> (Int,Int) -> IO Simplex
extend s v t tindex (lo, hi) = if (lo > hi)
                        then return s
                        else do vv <- readArray v lo
                                extendOne s vv (t !! (tindex + lo - 2))
                                extend s v t tindex (lo+1, hi)
                        

extendOne :: Simplex -> Vertex -> Triangle -> IO Vertex
extendOne s v t = do writeArray (get ngh (get tri s)) (get o s) (Just t)
                     let vertices = (get vtx (get tri s))
                     v1 <- readArray vertices (get o s)
                     v2 <- readArray vertices $ mod3 $ (get o s) + 2
                     setV t v1 v2 v 
                     setT t Nothing (Just (get tri s)) Nothing
                     return $ set inT (Just t) v

--incrementallyAddPoints :: IOArray Int Vertex -> Int -> Vertex
{-incrementallyAddPoints v n start = do let maxR  = truncate $ (n / 100) + 1
                                      let qqs   = newArray_ (1, maxR) 
                                      let qs    = newArray_ (1, maxR) 
                                      let t     = newArray_ (1, maxR)
                                      let flags = newArray_ (1, maxR)
                                      let h     = newArray_ (1, maxR)
  -}                                                                          




--delaunay :: [Point] -> [Triangle] 
--delaunay xs = 


