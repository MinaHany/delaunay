module Helpers where

import System.Random
import Data.Array.IO
import Control.Monad

type Point  = (Double, Double)
type MinMax = ((Double, Double), (Double, Double)) -- ((xmin, ymin), (xmax, ymax))

(|+|) :: Point -> Point -> Point
(x1, y1) |+| (x2, y2) = (x1 + x2, y1 + y2)

(|-|) :: Point -> Point -> Point
(x1, y1) |-| (x2, y2) = (x1 - x2, y1 - y2)

(|*|) :: Point -> Double -> Point
(x, y) |*| s = (x * s, y * s)

(|/|) :: Point -> Double -> Point
(x, y) |/| s = (x / s, y / s)

mod3 :: Int -> Int
mod3 x = if x > 2 then x - 3 else x 

distSort :: Point -> Point -> Point -> Ordering
distSort p p1 p2 | d1 < d2   = LT
                 | d1 > d2   = GT
                 | otherwise = EQ
    where d1 = distance p p1
          d2 = distance p p2

length :: Point -> Double
length (x,y) = sqrt $ x^2 + y^2

distance :: Point -> Point -> Double
distance (x1,y1) (x2,y2) = sqrt $ (x2 - x1)^2 + (y2 - y1)^2

nearest :: Point -> [Point] -> Point
nearest p (x:xs) = nearest' p xs x $ distance x p

nearest' :: Point -> [Point] -> Point -> Double -> Point
nearest' p []     minP min = minP 
nearest' p (x:xs) minP min = if d < min 
                                then nearest' p xs x    d
                                else nearest' p xs minP min
    where d = distance x p
    
minmax :: [Point] -> MinMax
minmax xs = minmax' xs (fst $ head xs) (snd $ head xs) (fst $ head xs) (snd $ head xs)

minmax' :: [Point] -> Double -> Double -> Double -> Double -> MinMax
minmax' []     xmin ymin xmax ymax = ((xmin, ymin), (xmax, ymax))
minmax' (x:xs) xmin ymin xmax ymax = minmax' xs xmin' ymin' xmax' ymax'
    where
        xmin' = min xmin $ fst x
        ymin' = min ymin $ snd x
        xmax' = max xmax $ fst x
        ymax' = max ymax $ snd x   
        
shuffle :: [Point] -> IO [Point]
shuffle xs = do
        ar <- newArray n xs
        forM [1..n] $ \i -> do
            j  <- randomRIO (i,n)
            vi <- readArray ar i
            vj <- readArray ar j
            writeArray ar j vi
            return vj
  where
    n = Prelude.length xs
    newArray :: Int -> [a] -> IO (IOArray Int a)
    newArray n xs =  newListArray (1,n) xs         
