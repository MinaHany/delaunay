#!/usr/bin/perl -w

use List::Util qw(min max);
use constant { true => 1, false => 0 };

sub triangle{
	($i, $j, $k, @nVertices) = @_;
	@a= $nVertices[$i];
	@b= $nVertices[$j];
	@c= $nVertices[$k];
	$A= $b[0] - $a[0];
	$B= $b[1] - $a[1];
	$C= $c[0] - $a[0];
	$D= $c[1] - $a[1];
	$E= $A * ($a[0] + $b[0]) + $B * ($a[1] + $b[1]);
	$F= $C * ($a[0] + $c[0]) + $D * ($a[1] + $c[1]);
	$G= 2 * ($A * ($c[1] - $b[1]) - B * ($c[0] - $b[0]));
	
	if(abs G < 0.000001){
		$minx= min [$a[0], $b[0], $c[0]];
		$miny= min [$a[1], $b[1], $c[1]];
		$maxx= max [$a[0], $b[0], $c[0]];
		$maxy= max [$a[1], $b[1], $c[1]];
		
		$dx= ($maxx - $minx) * 0.5;
		$dy= ($maxy - $miny) * 0.5; 
		$x = $minx + $dx;
		$y = $miny + $dy;
	}
	else{
		$x = ($D*$E - $B*$F) / $G;
		$y = ($A*$F - $C*$E) / $G;
		$dx= $x - $a[0];
		$dy= $y - $a[1];
	}
	$r= $dx * $dx + $dy * $dy;
	
	return ("i" => $i, "j" => $j, "k" => $k, "x" => $x, "y" => $y, "r" => $r);
}

sub vertexEq{
	@v1= @{$_[0]};
	@v2= @{$_[1]};
	return ($v1[0] == $v2[0] && $v1[1] == $v2[1]);
}

sub edgeEq{
	@e1= @{$_[0]};
	@e2= @{$_[1]};
	return ((vertexEq $e1[0], $e2[0] && vertexEq $e1[1], $e2[1]) 
			|| 
			(vertexEq $e1[0], $e2[1] && vertexEq $e1[1], $e2[0]));
}

sub appendSuper{
	@sVertices= @_;
	$minx= $v[0][0]; 
	$miny= $v[0][1];
	$maxx= $v[0][0];
	$maxy= $v[0][1];
	foreach $v (@sVertices){
		@v= @{$v};
		$minx= $v[0] if $v[0] < $minx;
		$miny= $v[1] if $v[1] < $miny;
		$maxx= $v[0] if $v[0] > $maxx;
		$maxy= $v[1] if $v[1] > $maxy;
	}
	
	$dx= $maxx - $minx;
	$dy= $maxy - $miny;
	if($dx > $dy){
		$dmax= $dx;
	}
	else{
		$dmax= $dy;
	}
	$midx= $minx + $dx * 0.5;
	$midy= $miny + $dy * 0.5;
	
	push(@sVertices, ($midx - 20 * $dmax, $midy -      $dmax));
	push(@sVertices, ($midx             , $midy + 20 * $dmax));
	push(@sVertices, ($midx + 20 * $dmax, $midy -      $dmax));
	
	return @sVertices;	
}

sub sorty{
	$a[0] <=> $b[0];
}

sub triangulate{
	@tVertices= @_;
	$n= $#vertices;
	return () if ($n < 3);
	
	@sortedV= sort sorty @tVertices;
	@tVertices= appendSuper @tVertices;
	
	@open= (triangle $n+0, $n+1, $n+2, @tVertices);
	@closed= ();
	@edges= ();
	
	foreach $v (@sortedV){
		@v= @{$v};
		foreach $op (@open){
			%op= %{$op};
			$dx= $v[0] - $op{"x"};
			if($dx > 0.0 && $dx * $dx > $op{"r"}){
				push(@closed, $op);
			}	
		}
	}
}



