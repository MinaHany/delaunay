#!/usr/bin/perl -w

$xmin= 1;
$xmax= 8;
$ymin= 2;
$ymax= 8;

$dx= $xmax - $xmin;
$dy= $ymax - $ymin;
if($dx > $dy){
	$dmax= $dx;
}
else{
	$dmax= $dy;
}

$xmid= $xmin + $dx * 0.5;
$ymid= $ymin + $dy * 0.5;

@v1= ($xmid - 20 * $dmax, $ymid -      $dmax);
@v2= ($xmid             , $ymid + 20 * $dmax);
@v3= ($xmid + 20 * $dmax, $ymid -      $dmax);

print "@v1\n@v2\n@v3\n";
